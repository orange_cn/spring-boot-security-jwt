package com.chenhao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("t_user")
@Data
public class UserDO {
    public static final String ADMIN = "admin";


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String username;
    private String name;
    private String tel;
}
