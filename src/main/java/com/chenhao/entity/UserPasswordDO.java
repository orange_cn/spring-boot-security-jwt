package com.chenhao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_user_password")
public class UserPasswordDO {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Long userId;
    private String password;
}
