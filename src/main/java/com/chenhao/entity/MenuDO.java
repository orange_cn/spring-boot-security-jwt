package com.chenhao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "t_menu")
public class MenuDO {
    private Integer id;
    private String name;
    private String url;
    private String perms;
}
