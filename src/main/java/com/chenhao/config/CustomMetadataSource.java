package com.chenhao.config;

import com.chenhao.entity.MenuDO;
import com.chenhao.entity.UserDO;
import com.chenhao.security.JwtUserDetails;
import com.chenhao.service.MenuService;
import com.chenhao.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;

/**
 * 自定义过滤器,请求的url需要什么权限
 */
@Component
public class CustomMetadataSource implements FilterInvocationSecurityMetadataSource {
    @Autowired
    private MenuService menuService;

    AntPathMatcher antPathMatcher = new AntPathMatcher();


    /**
     * @param o
     * @return 返回 null 的话，权限的拦截就交给Controller配置的PreAuthorize来拦截了，比如：@PreAuthorize("hasAuthority('school:user:view')")
     * 如果数据库menu中查询到请求url有对应的perms，接下来先由AccessDecisionManager的子类UrlAccessDecisionManager进行处理，
     * 将本处返回单的perms和数据库中url对应的perms进行比对，如果不匹配，就直接不让用户访问。
     */
    @Override
    public Collection<ConfigAttribute> getAttributes(Object o) throws IllegalArgumentException {
        //此处可放行超管
        JwtUserDetails currentUser = UserUtils.getCurrentUserModel();
        if (currentUser != null
                && !StringUtils.isEmpty(currentUser.getUsername())
                && currentUser.getUsername().equals(UserDO.ADMIN)
        ) {
            //  对于超管，可以直接访问所有url（对应的，Controller对于超管不做拦截，在UserDetailsService对List<GrantedAuthority>提前对超管账号进行了配置）
            return null;
        }
        String requestUrl = ((FilterInvocation) o).getRequestUrl();
        if (requestUrl.equals("/error")) {
            return null;
        }
        List<MenuDO> allMenu = menuService.getAllMenu();
        for (MenuDO menu : allMenu) {
            if (!StringUtils.isEmpty(menu.getUrl())){
                if (antPathMatcher.match(menu.getUrl(), requestUrl) /*&& menu.getRoles().size() > 0*/) {
                    // 获取url对应的perms
                    String perms = menu.getPerms();
                    if (StringUtils.isEmpty(perms)) {
                        return null;
                    } else {
                        String[] permNames = perms.split(",");
                        return SecurityConfig.createList(permNames);
                    }
                }
            }
        }
        // 没有匹配上的资源，都默认传递到Controller层进行处理。
        return null;
//        //没有匹配上的资源，都是登录访问
//        return SecurityConfig.createList("ROLE_LOGIN");
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return FilterInvocation.class.isAssignableFrom(aClass);
    }
}
