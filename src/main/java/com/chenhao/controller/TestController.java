package com.chenhao.controller;

import com.chenhao.entity.UserDO;
import com.chenhao.security.JwtAuthenticatioToken;
import com.chenhao.security.JwtAuthenticationRequest;
import com.chenhao.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
//@RequestMapping(value = "/auth")
public class TestController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    /**
     * 用户登录接口
     * @param authenticationRequest
     * @param request
     * @return
     */
    @PostMapping(value = "/login")
    public Object login(@RequestBody JwtAuthenticationRequest authenticationRequest, HttpServletRequest request) {
        if (StringUtils.isEmpty(authenticationRequest.getPassword()) || StringUtils.isEmpty(authenticationRequest.getUsername())){
            return "用户名或密码为空";
        }
        JwtAuthenticatioToken token = new JwtAuthenticatioToken(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        token.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        // 执行登录认证过程
        Authentication authentication = authenticationManager.authenticate(token);
        // 认证成功存储认证信息到上下文
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return userDetailsService.processLogin(authenticationRequest);
    }

    @GetMapping(value = "/home")
    public String home(){
        return "home";
    }

    @GetMapping(value = "/user")
    public String user(){
        return "user";
    }


    @GetMapping(value = "/setting")
    public String setting(){
        return "setting";
    }

}
