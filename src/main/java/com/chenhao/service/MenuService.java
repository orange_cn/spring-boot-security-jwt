package com.chenhao.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chenhao.entity.MenuDO;
import com.chenhao.mapper.MenuDOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MenuService {
    @Autowired
    private MenuDOMapper menuDOMapper;

    public List<MenuDO> getAllMenu() {
        return menuDOMapper.selectList(new QueryWrapper<MenuDO>());
    }

    public Set<String> findPermissions(Long userId){
        Set<String> permissions = new HashSet<>();
        permissions.add("sys");
        return permissions;
    }
}
