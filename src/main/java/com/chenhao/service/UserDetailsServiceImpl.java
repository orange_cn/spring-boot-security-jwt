package com.chenhao.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chenhao.entity.UserDO;
import com.chenhao.entity.UserPasswordDO;
import com.chenhao.mapper.UserDOMapper;
import com.chenhao.mapper.UserPasswordDOMapper;
import com.chenhao.security.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.security.SignatureException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserDOMapper userDOMapper;
    @Autowired
    private UserPasswordDOMapper userPasswordDOMapper;
    @Autowired
    private MenuService menuService;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDO userDO = userDOMapper.selectOne(new QueryWrapper<UserDO>().lambda().eq(UserDO::getUsername, username));
        if (userDO == null) {
            throw new UsernameNotFoundException("未找到用户");
        }
        Set<String> permissions = menuService.findPermissions(userDO.getId());
        List<GrantedAuthority> grantedAuthorities = null;
        if (!CollectionUtils.isEmpty(permissions)) {
            grantedAuthorities = permissions.stream().map(GrantedAuthorityImpl::new).collect(Collectors.toList());
        }
        UserPasswordDO userPasswordDO = userPasswordDOMapper.selectOne(new QueryWrapper<UserPasswordDO>().lambda().eq(UserPasswordDO::getUserId, userDO.getId()));
        return new JwtUserDetails(userDO.getId(), userDO.getName(), userPasswordDO.getPassword(), grantedAuthorities);
    }

    public JwtAuthenticationResponse processLogin(JwtAuthenticationRequest request) {
        if (request == null){
            return null;
        }
        JwtAuthenticationResponse response = new JwtAuthenticationResponse();
        BeanUtils.copyProperties(request,response);
        UserDO userDO = userDOMapper.selectOne(new QueryWrapper<UserDO>().lambda().eq(UserDO::getUsername, request.getUsername()));
        String token = tokenHead + jwtTokenUtil.generateToken(userDO.getId(), userDO.getUsername()); // 用户可能会使用手机号登录，此处转换为用户真正的userName
        response.setToken(token);
        return response;
    }

}
