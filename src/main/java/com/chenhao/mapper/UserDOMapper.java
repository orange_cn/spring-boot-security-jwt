package com.chenhao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chenhao.entity.UserDO;

public interface UserDOMapper extends BaseMapper<UserDO> {
}
