package com.chenhao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chenhao.entity.UserPasswordDO;

public interface UserPasswordDOMapper extends BaseMapper<UserPasswordDO> {
}
