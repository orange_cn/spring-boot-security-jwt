package com.chenhao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chenhao.entity.MenuDO;

public interface MenuDOMapper extends BaseMapper<MenuDO> {
}
