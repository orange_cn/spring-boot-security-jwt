package com.chenhao.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public class JwtUserDetails implements UserDetails {

    public JwtUserDetails(Long userId, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        this.id = userId;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    private Long id;
    private String username;
    private String password;
    private final Date lastPasswordResetDate = null;
    private Collection<? extends GrantedAuthority> authorities;


    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
