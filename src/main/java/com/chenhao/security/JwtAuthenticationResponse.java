package com.chenhao.security;

import lombok.Data;

@Data
public class JwtAuthenticationResponse extends JwtAuthenticationRequest {

    private String token;
}
