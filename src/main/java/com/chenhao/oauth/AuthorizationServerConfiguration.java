package com.chenhao.oauth;

import com.chenhao.service.UserDetailsServiceImpl;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpoint;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * An instance of Legacy Authorization Server (spring-security-oauth2) that uses a single,
 * not-rotating key and exposes a JWK endpoint.
 * <p>
 * See
 * <a
 * target="_blank"
 * href="https://docs.spring.io/spring-security-oauth2-boot/docs/current-SNAPSHOT/reference/htmlsingle/">
 * Spring Security OAuth Autoconfig's documentation</a> for additional detail
 *
 * @author Josh Cummings
 * @since 5.1
 */
@EnableAuthorizationServer
@Configuration
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {
    public static final String USER_RESOURCE_ID = "user-resource";

    @Autowired
    @Qualifier("authenticationManagerBean")
    AuthenticationManager authenticationManager;

    KeyPair keyPair;
    boolean jwtEnabled;

    @Value("${spring.security.oauth2.resourceserver.jwt.jwk-set-uri}")
    String jwkSetUri;

    @Bean
    JwtDecoder jwtDecoder() {
        return NimbusJwtDecoder.withJwkSetUri(this.jwkSetUri).build();
    }

    public AuthorizationServerConfiguration(KeyPair keyPair,
                                            @Value("${security.oauth2.authorizationserver.jwt.enabled:true}") boolean jwtEnabled) throws Exception {

        this.keyPair = keyPair;
        this.jwtEnabled = jwtEnabled;
    }

    //    @Bean
//    @Primary
//    @ConfigurationProperties(prefix = "spring.datasource")
//    public DataSource dataSource() {
//        return DataSourceBuilder.create().type(com.alibaba.druid.pool.DruidDataSource.class).build();
//    }
    @Autowired
    private DataSource dataSource;

    @Bean
    public ClientDetailsService jdbcClientDetails() {
        // 基于 JDBC 实现，需要事先在数据库配置创建oauth_client_details表。参考：https://github.com/spring-projects/spring-security-oauth/blob/master/spring-security-oauth2/src/test/resources/schema.sql
//        return new JdbcClientDetailsService(dataSource());
        return new JdbcClientDetailsService(dataSource);
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients)
            throws Exception {
        // 读取客户端配置
        clients.withClientDetails(jdbcClientDetails());
//        // @formatter:off
//        clients.inMemory()
//                .withClient("reader")
//                .authorizedGrantTypes("authorization_code", "password", "refresh_token", "implicit", "client_credentials")
//                .redirectUris("http://localhost:8091/qz/test")
//                .secret("{noop}secret")
//                .scopes("message:read")
//                .accessTokenValiditySeconds(120) // DefaultTokenServices中默认12小时
//                .refreshTokenValiditySeconds(120) // DefaultTokenServices中默认30天
//                .and()
//                .withClient("writer")
//                .authorizedGrantTypes("password")
//                .secret("{noop}secret")
//                .scopes("message:write")
//                .accessTokenValiditySeconds(600_000_000)
//                .and()
//                .withClient("noscopes")
//                .authorizedGrantTypes("password")
//                .secret("{noop}secret")
//                .scopes("none")
//                .accessTokenValiditySeconds(600_000_000);
//        // @formatter:on
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        // real 值可自定义
        oauthServer
//                .realm("spring-oauth-server") // real 值可自定义(自定义oauth prefix路径)
                // 支持 client_credentials 的配置
                .allowFormAuthenticationForClients();
    }


    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        // @formatter:off
        endpoints
                .authenticationManager(this.authenticationManager)
                .userDetailsService(userDetailsService) // 跟刷新token有关，参见：UserDetailsByNameServiceWrapper
                .tokenStore(tokenStore());

        if (this.jwtEnabled) {
            endpoints
                    .accessTokenConverter(accessTokenConverter());
        }
        // @formatter:on
    }

    @Bean
    public TokenStore tokenStore() {
        if (this.jwtEnabled) {
            return new JwtTokenStore(accessTokenConverter());
        } else {
            return new InMemoryTokenStore();
        }
    }

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setKeyPair(this.keyPair);

        DefaultAccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverter();
        SubjectAttributeUserTokenConverter subjectAttributeUserTokenConverter = new SubjectAttributeUserTokenConverter();
        subjectAttributeUserTokenConverter.setUserDetailsService(userDetailsService);
        accessTokenConverter.setUserTokenConverter(subjectAttributeUserTokenConverter);
        converter.setAccessTokenConverter(accessTokenConverter);

        return converter;
    }


    // user resource
    @Configuration
    @EnableResourceServer
    protected static class QzOauth2ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
//        @Autowired
//        CustomMetadataSource metadataSource;
//        @Autowired
//        UrlAccessDecisionManager urlAccessDecisionManager;
//        @Autowired
//        @Qualifier("authenticationManagerBean")
//        AuthenticationManager authenticationManager;
//        @Autowired
//        ClientDetailsService clientDetailsService;

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) {
            resources.resourceId(USER_RESOURCE_ID).stateless(false);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
//            http.setSharedObject(ClientDetailsService.class, clientDetailsService);
            http
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                    .and().requestMatchers().antMatchers("/oauthRes/**")
                    .and().authorizeRequests()
//                    .mvcMatchers("/.well-known/jwks.json").permitAll()
                    .antMatchers("/oauthRes/**").authenticated()
//                    .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
//                        @Override
//                        public <O extends FilterSecurityInterceptor> O postProcess(O o) {
//                            o.setSecurityMetadataSource(metadataSource);
//                            o.setAccessDecisionManager(urlAccessDecisionManager);
//                            return o;
//                        }
//                    })
            // Since we want the protected resources to be accessible in the UI as well we need
            // session creation to be allowed (it's disabled by default in 2.0.6)
//                    .and().httpBasic()
//                    .and().formLogin()
//                    .loginPage("/login")
//                    .loginProcessingUrl("/qz/login")
//                .and().csrf().ignoringRequestMatchers(request -> "/introspect".equals(request.getRequestURI()))
//                    .and().oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt).oauth2ResourceServer().bearerTokenResolver(allowRequestBody())
            ;
        }

//        /**
//         * 允许access_token在请求参数中出现
//         *
//         * @return
//         */
//        private BearerTokenResolver allowRequestBody() {
//            DefaultBearerTokenResolver resolver = new DefaultBearerTokenResolver();
//            resolver.setAllowFormEncodedBodyParameter(true);
//            resolver.setAllowUriQueryParameter(true);
//            return resolver;
//        }

    }
}

/**
 * Legacy Authorization Server (spring-security-oauth2) does not support any
 * <a href target="_blank" href="https://tools.ietf.org/html/rfc7517#section-5">JWK Set</a> endpoint.
 * <p>
 * This class adds ad-hoc support in order to better support the other samples in the repo.
 */
@FrameworkEndpoint
class JwkSetEndpoint {
    KeyPair keyPair;

    JwkSetEndpoint(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    @GetMapping("/.well-known/jwks.json")
    @ResponseBody
    public Map<String, Object> getKey() {
        RSAPublicKey publicKey = (RSAPublicKey) this.keyPair.getPublic();
        RSAKey key = new RSAKey.Builder(publicKey).build();
        return new JWKSet(key).toJSONObject();
    }
}

/**
 * An Authorization Server will more typically have a key rotation strategy, and the keys will not
 * be hard-coded into the application code.
 * <p>
 * For simplicity, though, this sample doesn't demonstrate key rotation.
 */
@Configuration
class KeyConfig {
    @Bean
    KeyPair keyPair() {
        try {
            String privateExponent = "3851612021791312596791631935569878540203393691253311342052463788814433805390794604753109719790052408607029530149004451377846406736413270923596916756321977922303381344613407820854322190592787335193581632323728135479679928871596911841005827348430783250026013354350760878678723915119966019947072651782000702927096735228356171563532131162414366310012554312756036441054404004920678199077822575051043273088621405687950081861819700809912238863867947415641838115425624808671834312114785499017269379478439158796130804789241476050832773822038351367878951389438751088021113551495469440016698505614123035099067172660197922333993";
            String modulus = "18044398961479537755088511127417480155072543594514852056908450877656126120801808993616738273349107491806340290040410660515399239279742407357192875363433659810851147557504389760192273458065587503508596714389889971758652047927503525007076910925306186421971180013159326306810174367375596043267660331677530921991343349336096643043840224352451615452251387611820750171352353189973315443889352557807329336576421211370350554195530374360110583327093711721857129170040527236951522127488980970085401773781530555922385755722534685479501240842392531455355164896023070459024737908929308707435474197069199421373363801477026083786683";
            String exponent = "65537";

            RSAPublicKeySpec publicSpec = new RSAPublicKeySpec(new BigInteger(modulus), new BigInteger(exponent));
            RSAPrivateKeySpec privateSpec = new RSAPrivateKeySpec(new BigInteger(modulus), new BigInteger(privateExponent));
            KeyFactory factory = KeyFactory.getInstance("RSA");
            return new KeyPair(factory.generatePublic(publicSpec), factory.generatePrivate(privateSpec));
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}

/**
 * Legacy Authorization Server does not support a custom name for the user parameter, so we'll need
 * to extend the default. By default, it uses the attribute {@code user_name}, though it would be
 * better to adhere to the {@code sub} property defined in the
 * <a target="_blank" href="https://tools.ietf.org/html/rfc7519">JWT Specification</a>.
 */
class SubjectAttributeUserTokenConverter extends DefaultUserAuthenticationConverter {
    @Override
    public Map<String, ?> convertUserAuthentication(Authentication authentication) {
        Map<String, Object> response = new LinkedHashMap<>();
//        response.put("sub", authentication.getName());
        response.put(UserAuthenticationConverter.USERNAME, authentication.getName()); // 使用标准方式，为了能够后续在API中得到登录用户信息
        if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
            response.put(AUTHORITIES, AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
        }
        return response;
    }
}

