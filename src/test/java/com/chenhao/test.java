package com.chenhao;

import java.util.ArrayList;
import java.util.List;

public class test {

    public static <F> List<F> datepaging(List<F> f, int pageNo, int dataSize) {
        if (f == null) {// 当传入过来的 list 集合为 null 时，先进行实例化
            f = new ArrayList<F>();
        }
        if (pageNo <= 0) {
            pageNo = 1;
        }
        int totalitems = f.size();
        List<F> afterList = new ArrayList<F>();
        for (int i = (pageNo - 1) * dataSize;
             i < (Math.min(((pageNo - 1) * dataSize) + dataSize, totalitems)); i++) {
            afterList.add(f.get(i));
        }
        return afterList;
    }
}
