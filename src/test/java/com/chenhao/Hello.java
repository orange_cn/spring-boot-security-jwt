package com.chenhao;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Hello {

    @Test
    public void passwordEncoder() {
        String passwd = "123456";
        String encode = new BCryptPasswordEncoder().encode(passwd);
        System.out.println("加密后的密码：");
        System.out.println(encode);
    }
}
